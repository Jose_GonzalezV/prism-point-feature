﻿using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Drawing;
using System;
using System.Windows.Data;

namespace WpfApplication2
{
    public  class ViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Point> points { get; set; }
        public ObservableCollection<Point> missPoints { get; set; }
        private static object _lock = new object();
        public ObservableCollection<Point> okToDrill { get; set; } // TESTING ONLY. list might need to be added
        public event PropertyChangedEventHandler PropertyChanged;

       

        public ViewModel():base()
        {
            
            points = new ObservableCollection<Point>();
            missPoints = new ObservableCollection<Point>();
            BindingOperations.EnableCollectionSynchronization(missPoints, _lock);
            this.testData();
            RaisePropertyChanged("missPoints");
        }


        public void RaisePropertyChanged(string propertyName)
        {
            var pc = PropertyChanged;
            if (pc != null)
                pc(this, new PropertyChangedEventArgs(propertyName));
        }


        void handleInput(object sender, EventArgs e)
        {
            Console.WriteLine("The threshold was reached.");
            Environment.Exit(0);
        }





        /// <summary>
        /// enters test data
        /// </summary>
        private void testData()
        {
            
            points.Add(new Point(0, 50));
            points.Add(new Point(50, 0));
            points.Add(new Point(13, 73));
            points.Add(new Point(12, 23));
            points.Add(new Point(34, 80));
            points.Add(new Point(322, 225));
            points.Add(new Point(270, 510));
            points.Add(new Point(100, 0));
            
            points.Add(new Point(400, 400));
            points.Add(new Point(420, 123));
            points.Add(new Point(345, 273));
            points.Add(new Point(238, 23));
            points.Add(new Point(456, 80));
            points.Add(new Point(321, 225));
            points.Add(new Point(124, 411));
            points.Add(new Point(492, 0));

            missPoints.Add(new Point(50, 50));
            missPoints.Add(new Point(75, 25));
            missPoints.Add(new Point(15, 73));
            missPoints.Add(new Point(135, 412));
            missPoints.Add(new Point(134, 240));
            missPoints.Add(new Point(300, 225));
            missPoints.Add(new Point(290, 470));
            missPoints.Add(new Point(179, 453));
        }


    }
}
